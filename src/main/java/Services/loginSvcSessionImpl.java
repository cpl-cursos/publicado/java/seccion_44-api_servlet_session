package Services;

import jakarta.servlet.http.HttpServletRequest;

import java.util.Optional;

public class loginSvcSessionImpl implements loginSvc{
    @Override
    public Optional<String> getUsuario(HttpServletRequest req) {
        return ((String) req.getSession().getAttribute("usuario") != null)
                ? Optional.of((String) req.getSession().getAttribute("usuario"))
                : Optional.empty();
    }
}
