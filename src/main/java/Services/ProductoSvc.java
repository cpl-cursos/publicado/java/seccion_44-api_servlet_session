package Services;

import modelos.Producto;

import java.util.List;

public interface ProductoSvc {
    List<Producto> listar();
}
