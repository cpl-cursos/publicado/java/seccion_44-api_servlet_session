package controllers;

import Services.loginSvc;
import Services.loginSvcImpl;
import Services.loginSvcSessionImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@WebServlet("/salir")
public class salirSrvlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        loginSvc aut = new loginSvcSessionImpl();
        Optional<String> usuopt = aut.getUsuario(req);
        // ... si existe, el usuario es válido ...
        if (usuopt.isPresent()) {
            req.getSession().invalidate();
        }
        resp.sendRedirect(req.getContextPath()+"/acceso");
    }
}
