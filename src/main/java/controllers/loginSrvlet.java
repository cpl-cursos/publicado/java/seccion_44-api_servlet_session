package controllers;

import Services.loginSvc;
import Services.loginSvcImpl;
import Services.loginSvcSessionImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Optional;

@WebServlet("/acceso")
public class loginSrvlet extends HttpServlet {
    final static String USUARIO = "admin";
    final static String CLAVE = "12345";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        loginSvc aut = new loginSvcSessionImpl();
        Optional<String> usuopt = aut.getUsuario(req);
        // ... si existe, es que el usuario ha accedido previamente ...
        if (usuopt.isPresent()) {
            // ... presentamos un mensaje indicándoselo al usuario
            resp.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = resp.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("    <head>");
                out.println("        <meta charset=\"UTF-8\">");
                out.println("        <title>Acceso válido</title>");
                out.println("    </head>");
                out.println("    <body>");
                out.println("        <h1>Hola " + usuopt.get() +"</h1>");
                out.println("<h3>Bienvenido " + usuopt.get() +" ya has accedido previamente y está identificado.</h3>");
                out.println("    </body>");
                out.println("</html>");
            }
        } else {
            // La cookie NO EXISTE, por lo que el usuario no se ha identificado correctamente. Error y vuelta al login
            getServletContext().getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Leemos los datos del login
        String usu = req.getParameter("usuario");
        String clave = req.getParameter("clave");
        resp.setContentType("text/html;charset=UTF-8");

        // Comprobamos que los datos coinciden con el usuario almacenado
        if (USUARIO.equals(usu) && CLAVE.equals(clave)) {

            req.getSession().setAttribute("usuario", usu);

            // Presentamos el mensaje indicando el acceso válido
            try (PrintWriter out = resp.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("    <head>");
                out.println("        <meta charset=\"UTF-8\">");
                out.println("        <title>Login</title>");
                out.println("    </head>");
                out.println("    <body>");
                out.println("        <h1>Acceso correcto</h1>");
                out.println("<h3>Hola " + usu + ". Bienvenido. Dime que deseas.</h3>");
                out.println("</ul>");
                out.println("    </body>");
                out.println("</html>");
            }
        } else {
            // Los datos no son correctos. Error.
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Lo siento. El usuario y/o la clave son erroneos.");
        }
    }
}
