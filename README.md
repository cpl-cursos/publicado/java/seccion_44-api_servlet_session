Código de los ejemplos realizados en clase
==========================================

Partiendo del código inicial, en los sucesivos commits se van añadiendo el código mostrado en cada uno de los apartados.

La configuración de ejecución es con maven y tomcat7:redeploy.

Para ver cómo evoluciona el código en sus distintas etapas, revisar cada uno de los commits.